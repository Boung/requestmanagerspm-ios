<h1> APIRequestManagerSPM </h1>

APIRequestManagerSPM is a simple SPM for API request with URLSession.
There are some functions included on this SPM
<ul>
  <li> DataTask Request </li>
  <li> DownloadTask Requesta </li>
  <li> UploadTask Request </li>
</ul>

<h2> DataTask Request </h2>
DataTask Request purposely use for request normal API request which has a 2 callbacks for <b>Codable Response</b> and <b>Error</b>.

<h3> Function </h3>

```
func fetch<T: Codable>(_ type: T.Type, request: RequestProtocol, preferredLoadingView: Bool = true, retryAttempt: Int = 3, onSuccess: CallbackType<T?>?, onFailure: CallbackType<RequestError>? = nil) { }
```

<h3> Properties </h3>

```
// This property is for LoadingViewController. 
// Datatype: UIViewController
// DefaultValue: DefaultLoadingViewController()
APIRequestManager.shared.preferredLoadingViewController

// This property is for showing console Log.
// Datatype: Bool
// DefaultValue: true
APIRequestManager.shared.preferredShowRequestLog 
```

<h3> Usage </h3>

```
APIRequestManager.shared.preferredLoadingViewController = UIViewController()
APIRequestManager.shared.preferredShowRequestLog = true
```

```
enum APIRequest: RequestProtocol {
  case get
  
  var baseURL: String { "YOUR BASEURL" }
  var endPoint: String { "YOUR ENDPOINT" }
  var httpHeader: APIRequestManagerSPM.HTTPHeader? { ["header": "header"] }
  var httpMethod: APIRequestManagerSPM.HTTPMethod { .get }
  var paramater: APIRequestManagerSPM.Paramater? { ["body": "body"] }
  var encoding: APIRequestManagerSPM.EncodingMethod { .urlEncoding }
  var requiredAuthentication: Bool { false }
}

class APIService {
  func requestAPI() {
    let request: APIRequest = .get
    
    APIRequestManager.shared.fetch(String.self, request: request, preferredLoadingView: true, retryAttempt: 3) { response in
    // Handle Success Response Here
    
    } onFailure: { error in
    // Handle Error Here
    
    }
  }
}
```

<h2> DownloadTask Request </h2>
DownloadTask Request is purposely use for download file from remote side which has 2 callbacks for <b>File's Data</b> and <b>Error</b>

<h3> Function </h3>

```
func download(request: RequestProtocol, preferredLoadingView: Bool = true, retryAttempt: Int = 3, onSuccess: CallbackType<Data>?, onFailure: CallbackType<RequestError>? = nil) { }
```

<h3> Properties </h3>

```
// This property is for LoadingViewController. 
// Datatype: UIViewController
// DefaultValue: DefaultLoadingViewController()
DownloadRequestManager.shared.preferredLoadingViewController

// This property is for showing console Log.
// Datatype: Bool
// DefaultValue: true
DownloadRequestManager.shared.preferredShowRequestLog

// This property is for showing completed download percentage in case we can't get the file size from the server.
// Datatype: Float
// DefaultValue: 30
DownloadRequestManager.shared.preferredNoFileSizePercentage

// This property is a callback for handling download progression.
// Datatype: Callback<Float>
// DefaultValue: nil
DownloadRequestManager.shared.downloadProgressHandler 
```

<h3> Usage </h3>

```
DownloadRequestManager.shared.preferredLoadingViewController = UIViewController()
DownloadRequestManager.shared.preferredShowRequestLog = true
DownloadRequestManager.shared.preferredNoFileSizePercentage = 50
DownloadRequestManager.shared.downloadProgressHandler = { downloadCompletedPercentage in 
  // Handle your action here with `downloadCompletedPercentage`
  
}

```

```
class DownloadRequest: RequestProtocol {
  var baseURL: String { "YOUR BASEURL" }
  var endPoint: String { "YOUR ENDPOINT" }
  var httpHeader: APIRequestManagerSPM.HTTPHeader? { nil }
  var httpMethod: APIRequestManagerSPM.HTTPMethod { .get }
  var paramater: APIRequestManagerSPM.Paramater? { nil }
  var encoding: APIRequestManagerSPM.EncodingMethod { .jsonEncoding }
  var requiredAuthentication: Bool { false }
}

class DownloadService {
  static func download(completion: CallbackType<Data>?) {
    DownloadRequestManager.shared.downloadProgressHandler = { percentage in
      print("Downloaded Percentage: \(percentage)")
    }
    DownloadRequestManager.shared.download(request: DownloadRequest(), preferredLoadingView: true, retryAttempt: 3) { data in
    // Handle Success Download Here
    
    } onFailure: { error in
    // Handle Error Here
     
    }
  }
}

```

<h2> UploadTask Request </h2>
Upload Task Request is purposely use for upload MultipartForm request to server side which has 2 callbacks for <b>Codable Response</b> and <b>Error</b>.

<h3> Function </h3>

```
func upload<T: Codable>(_ type: T.Type, request: MultipartRequestProtocol, preferredLoadingView: Bool = true, retryAttempt: Int = 3, onSuccess: CallbackType<T?>?, onFailure: CallbackType<RequestError>? = nil) { }
```

<h3> Properties </h3>

```
// This property is for LoadingViewController. 
// Datatype: UIViewController
// DefaultValue: DefaultLoadingViewController()
UploadRequestManager.shared.preferredLoadingViewController

// This property is for showing console Log.
// Datatype: Bool
// DefaultValue: true
UploadRequestManager.shared.preferredShowRequestLog 
```

<h3> Usage </h3>

```
UploadRequestManager.shared.preferredLoadingViewController = UIViewController()
UploadRequestManager.shared.preferredShowRequestLog = true
```

```
class UploadRequest: MultipartRequestProtocol {
  var baseURL: String { "YOUR BASEURL" }
  var endPoint: String { "YOUR ENDPOINT" }
  var httpHeader: APIRequestManagerSPM.HTTPHeader? { ["header": "header"] }
  var httpMethod: APIRequestManagerSPM.HTTPMethod { .get }
  var paramater: APIRequestManagerSPM.Paramater? { ["body": "body"] }
  var encoding: APIRequestManagerSPM.EncodingMethod { .urlEncoding }
  var requiredAuthentication: Bool { false }
  var boundary: String { "YOUR BOUNDARY" }
  var fileDatas: [APIRequestManagerSPM.MultipartParamater] {
    return [
      .init(file: UIImage(named: "")!.pngData()!, fileName: "FILENAME", name: "FILE", mimeType: .imagePNG),
    ]
  }
}

class UploadService {
  static func upload() {
    UploadRequestManager.shared.upload(String.self, request: MultipartRequest(), preferredLoadingView: true, retryAttempt: 3) { response in
    // Handle Success Response Here
    
    } onFailure: { error in
    // Handle Error Here
    
    }
  }
}
```

<h1> Demo Project </h1>
Please refer to this <a href="https://gitlab.com/Boung/demoapirequestmanagerspm">Demo Project</a>.

