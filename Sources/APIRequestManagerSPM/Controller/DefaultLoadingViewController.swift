//
//  DefaultLoadingViewController.swift
//  
//
//  Created by Ly Boung on 18/8/23.
//

import UIKit

class DefaultLoadingViewController: UIViewController {
  
  private lazy var indicator: UIActivityIndicatorView = {
    let indicator = UIActivityIndicatorView()
    indicator.tintColor = .white
    indicator.color = .white
    indicator.translatesAutoresizingMaskIntoConstraints = false
    
    return indicator
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    prepareLayouts()
    indicator.startAnimating()
  }
  
  deinit {
    indicator.stopAnimating()
  }
  
  private func prepareLayouts() {
    
    view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    
    view.addSubview(indicator)
    indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
  }
}
