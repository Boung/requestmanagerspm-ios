//
//  MimeType.swift
//  
//
//  Created by Ly Boung on 21/8/23.
//

import Foundation

/// MimeType for MultipartRequest
public enum MimeType {
  /// For image file with extension .png
  case imagePNG
  
  /// For image file with extension .jpeg/.jpg
  case imageJPEG
  
  /// For pdf file with extension .pdf
  case filePDF
  
  /// For any other type of file
  /// - Parameters:
  ///   - mimeType: String value that will pass to server (example: image/png or image/jpeg or application/pdf)
  ///   - fileExtension: String value that will pass to server for file extension (example: .png or .jpeg or .pdf)
  case custom(mimeType: String, fileExtension: String)
  
  var string: String {
    switch self {
      case .imagePNG:
        return "image/png"
      case .imageJPEG:
        return "image/jpeg"
      case .filePDF:
        return "application/pdf"
      case .custom(let value, _):
        return value
    }
  }
  
  var fileExtension: String {
    switch self {
      case .imagePNG:
        return ".png"
      case .imageJPEG:
        return ".jpeg"
      case .filePDF:
        return ".pdf"
      case .custom(_, let fileExtension):
        return fileExtension
    }
  }
}
