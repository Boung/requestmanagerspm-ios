//
//  EncodingMethod.swift
//  
//
//  Created by Boung on 20/8/23.
//

import Foundation

/// EncodingMethod Enum for paramater encoding
public enum EncodingMethod {
  /// For URL encoding - URL Query
  case urlEncoding
  
  /// For Body encoding - Body Paramater
  case jsonEncoding
}
