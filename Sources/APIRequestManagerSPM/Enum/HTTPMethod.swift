//
//  File.swift
//  
//
//  Created by Boung on 20/8/23.
//

import Foundation

/// HTTPMethod for request method
public enum HTTPMethod {
  /// GET Method
  case get
  
  /// PUT Method
  case put
  
  /// POST Method
  case post
  
  /// DELETE Method
  case delete
  
  /// CUSTOM Method
  /// - Parameters:
  ///   - value: String value that will use as request method (The value will be changed to Uppercased
  case custom(value: String)
  
  var string: String {
    switch self {
      case .get:
        return "GET"
      case .put:
        return "PUT"
      case .post:
        return "POST"
      case .delete:
        return "DELETE"
      case .custom(let value):
        return value.uppercased()
    }
  }
}
