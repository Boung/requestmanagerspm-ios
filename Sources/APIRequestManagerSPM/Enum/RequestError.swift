//
//  File.swift
//  
//
//  Created by Ly Boung on 18/8/23.
//

import Foundation

/// Request Error type for APIRequestManagerSPM
public enum RequestError: Error {
  
  /// For no network connection error
  case networkError
  
  /// For request callback with data EMPTY or NIL
  case noDataResponse
  
  /// For error with URLRequestError is NIL
  case urlRequestError
  
  /// For request callback with error is not NIL
  case requestError
  
  /// For request callback with BadRequest response
  case badRequest
  
  /// For request callback with Unauthorized response
  case unauthorized
  
  /// For request callback with Server Not Found - 404
  case serverNotFound
  
  /// For request callback with Internal Server Error
  case internalServerError
  
  /// For success callback with error decoding response model
  case decodingError
  
  /// For request cancel by user
  case requestCancelled
  
  /// For request timeout
  case requestTimeout
  
  /// For other error with message response
  case generalError(message: String?)
  
  public var errorMessage: String {
    switch self {
      case .networkError:
        return "No internet connection"
      case .noDataResponse:
        return "No Data response"
      case .urlRequestError:
        return "Invalid URLRequest"
      case .requestError:
        return "Request Error"
      case .badRequest:
        return "Base Request"
      case .unauthorized:
        return "Unauthorized"
      case .serverNotFound:
        return "Server not found"
      case .internalServerError:
        return "Internal server error"
      case .decodingError:
        return "Decode Model Error"
      case .requestCancelled:
        return "Request Cancelled"
      case .requestTimeout:
        return "Request timeout"
      case .generalError(let message):
        return message ?? "General error"
    }
  }
}
