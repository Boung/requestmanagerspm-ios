//
//  UploadRequestManager.swift
//  
//
//  Created by Ly Boung on 22/8/23.
//

import UIKit

public class UploadRequestManager: Request {
  private init() {
    self.session = URLSession.shared
  }
  
  var showRequestLog: Bool { preferredShowRequestLog }
  var loadingViewController: UIViewController { preferredLoadingViewController }
  private var session: URLSession
  private var task: URLSessionDataTask?
  
  public static let shared = UploadRequestManager()
  
  /// Loading ViewController for waiting
  public var preferredLoadingViewController: UIViewController = DefaultLoadingViewController()
  
  /// Boolean value for showing console Log
  public var preferredShowRequestLog: Bool = true
  
  /// Upload multipart request to server
  /// - Parameters:
  ///   - type: The expected response from API request - must extend Codable
  ///   - request: The object which will construct the request - must extend from MultipartRequestProtocol and overrided the necessary properties
  ///   - preferredLoadingView: Bool value which assgined to show loading controller during request
  ///   - retryAttempt: Number of retry attempt when request fail.
  ///   - onSuccess: Callback which return the Success Response
  ///   - onFailure: Callback which return the error type of RequestError
  /// - Returns: nil
  public func upload<T: Codable>(_ type: T.Type, request: MultipartRequestProtocol, preferredLoadingView: Bool = true, retryAttempt: Int = 3, onSuccess: CallbackType<T?>?, onFailure: CallbackType<RequestError>? = nil) {
    guard NetworkManager.isConnectedToNetwork() else {
      onFailure?(.networkError)
      return
    }
    
    guard let urlRequest = request.urlRequest else {
      onFailure?(.urlRequestError)
      return
    }
    
    showRequestLog(urlRequest, data: nil, statusCode: nil)
    showLoadingController(preferredLoadingView) {
      self.task = self.session.uploadTask(with: urlRequest, from: request.requestData) { data, response, error in
        self.hideLoadingController(preferredLoadingView) {
          if let error = error {
            let nsError = error as NSError
            
            if nsError.domain == NSURLErrorDomain {
              if nsError.code == NSURLErrorCancelled {
                onFailure?(.requestCancelled)
                return
              }
              
              if nsError.code == NSURLErrorTimedOut {
                onFailure?(.requestTimeout)
                return
              }
            }
            
            if retryAttempt > 0 {
              self.upload(type, request: request, preferredLoadingView: preferredLoadingView, retryAttempt: retryAttempt - 1, onSuccess: onSuccess, onFailure: onFailure)
              return
            }
            
            onFailure?(.requestError)
            return
          }
          
          guard let data = data, !data.isEmpty else {
            if retryAttempt > 0 {
              self.upload(type, request: request, preferredLoadingView: preferredLoadingView, retryAttempt: retryAttempt - 1, onSuccess: onSuccess, onFailure: onFailure)
              return
            }
            
            onFailure?(.noDataResponse)
            return
          }
          
          let httpResponse = response as? HTTPURLResponse
          let statusCode = httpResponse?.statusCode ?? 0
          
          self.showRequestLog(urlRequest, data: data, statusCode: statusCode)
          
          do{
            switch statusCode {
              case ServerCode.statusOk:
                let response = try JSONDecoder().decode(T.self, from: data)
                onSuccess?(response)
              case ServerCode.badRequest:
                onFailure?(.badRequest)
              case ServerCode.unauthorized:
                onFailure?(.unauthorized)
              case ServerCode.notFound:
                onFailure?(.serverNotFound)
              case ServerCode.internalServerError:
                onFailure?(.internalServerError)
              default:
                let errorMessage = "General Error - Code(\(statusCode))"
                onFailure?(.generalError(message: errorMessage))
            }
          }catch {
            onFailure?(.decodingError)
          }
        }
      }
      
      self.task?.resume()
    }
  }
  
  public func cancelTask() {
    guard let task = task else { return }
    if task.state == .running {
      task.suspend()
    }
    
    task.cancel()
  }
}
