//
//  DownloadRequestManager.swift
//  
//
//  Created by Ly Boung on 22/8/23.
//

import UIKit

public class DownloadRequestManager: NSObject, Request {
  public override init() {
    super.init()
  }
  
  var showRequestLog: Bool { preferredShowRequestLog }
  var loadingViewController: UIViewController { preferredLoadingViewController }
  var onSuccessHandler: CallbackType<Data>?
  var onFailureHandler: CallbackType<RequestError>?
  
  var session: URLSession?
  var task: URLSessionDownloadTask?
  
  public static let shared = DownloadRequestManager()
  
  /// Loading ViewController for waiting
  public var preferredLoadingViewController: UIViewController = DefaultLoadingViewController()
  
  /// Boolean value for showing console Log
  public var preferredShowRequestLog: Bool = true
  
  /// Float value use as default value in case can't get file size
  public var preferredNoFileSizePercentage: Float = 30
  
  /// Callback which return Float value of completed download percentage
  public var downloadProgressHandler: CallbackType<Float>?
  
  var preferredLoadingView: Bool = true
  
  /// Download file from server
  /// - Parameters:
  ///   - request: The object which will construct the request - must extend from RequestProtol and overrided the necessary properties
  ///   - preferredLoadingView: Bool value which assgined to show loading controller during request
  ///   - retryAttempt: Number of retry attempt when request fail.
  ///   - onSuccess: Callback which return the downloaded file Data
  ///   - onFailure: Callback which return the error type of RequestError
  /// - Returns: nil
  public func download(request: RequestProtocol, preferredLoadingView: Bool = true, retryAttempt: Int = 3, onSuccess: CallbackType<Data>?, onFailure: CallbackType<RequestError>? = nil) {
    guard NetworkManager.isConnectedToNetwork() else {
      onFailure?(.networkError)
      return
    }
    
    guard let urlRequest = request.urlRequest else {
      onFailure?(.urlRequestError)
      return
    }
    
    onSuccessHandler = onSuccess
    onFailureHandler = onFailure
    self.preferredLoadingView = preferredLoadingView
    showRequestLog(urlRequest, data: nil, statusCode: nil)
    
    session = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
    task = session?.downloadTask(with: urlRequest)
    
    showLoadingController(preferredLoadingView) {
      self.task?.resume()
    }
  }
  
  public func cancelTask() {
    guard let task = task else { return }
    if task.state == .running {
      task.suspend()
    }
    
    task.cancel()
  }
}

extension DownloadRequestManager: URLSessionDownloadDelegate {
  public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
    guard let error = error else { return }
    let nsError = error as NSError
    
    if nsError.domain == NSURLErrorDomain {
      if nsError.code == NSURLErrorCancelled {
        triggerErrorCallback(error: .requestCancelled)
        return
      }
      
      if nsError.code == NSURLErrorTimedOut {
        triggerErrorCallback(error: .requestTimeout)
        return
      }
    }
    
    triggerErrorCallback()
  }
  
  public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
    guard self.isRequestOK(response: downloadTask.response) else {
      triggerErrorCallback()
      return
    }
    
    do {
      let data = try Data(contentsOf: location)
      triggerSuccessCallback(data: data)
    }catch {
      triggerErrorCallback()
    }
  }
  
  public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
    guard totalBytesExpectedToWrite != NSURLSessionTransferSizeUnknown else {
      downloadProgressHandler?(preferredNoFileSizePercentage)
      return
    }
    
    let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
    downloadProgressHandler?(progress * 100)
  }
  
  private func isRequestOK(response: URLResponse?) -> Bool {
    guard let response = response else { return false }
    guard let httpResponse = response as? HTTPURLResponse else { return false }
    
    return ServerCode.statusOk.contains(httpResponse.statusCode)
  }
  
  private func triggerSuccessCallback(data: Data) {
    hideLoadingController(preferredLoadingView) { [weak self] in
      guard let self = self else { return }
      
      self.onSuccessHandler?(data)
      self.downloadProgressHandler?(100.0)
    }
  }
  
  private func triggerErrorCallback(error: RequestError = .requestError) {
    hideLoadingController(preferredLoadingView) { [weak self] in
      guard let self = self else { return }
      
      self.downloadProgressHandler?(0.0)
      self.onFailureHandler?(error)
    }
  }
}
