//
//  MultipartRequestProtocol.swift
//
//
//  Created by Boung on 20/8/23.
//

import Foundation

public protocol MultipartRequestProtocol {
  var baseURL: String { get }
  var endPoint: String { get }
  var httpHeader: HTTPHeader { get }
  var httpMethod: HTTPMethod { get }
  var paramater: Paramater? { get }
  var encoding: EncodingMethod { get }
  var requiredAuthentication: Bool { get }
  
  var boundary: String { get }
  var fileDatas: [MultipartParamater] { get }
  
  var timeoutInterval: TimeInterval { get }
}

public extension MultipartRequestProtocol {
  var timeoutInterval: TimeInterval { 60 }
}

public extension MultipartRequestProtocol {
  private var url: URL? {
    guard let urlString = (baseURL + endPoint).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return nil }
    guard let url = URL(string: urlString) else { return nil }
    
    return url
  }
  
  private var allowedBoundary: String {
    return "Boundary-\(boundary)"
  }
  
  var urlRequest: URLRequest? {
    guard let url = url else { return nil }
    
    var request = URLRequest(url: url)
    request.timeoutInterval = timeoutInterval
    request.httpMethod = httpMethod.string
    request.allHTTPHeaderFields = httpHeader
    request.setValue("multipart/form-data; boundary=\(allowedBoundary)", forHTTPHeaderField: "Content-Type")
    
    if let paramater = paramater, encoding == .jsonEncoding {
      if let data = try? JSONSerialization.data(withJSONObject: paramater, options: []) {
        request.httpBody = data
      }
    }
    
    return request
  }
  
  var requestData: Data {
    return getMultipartData(contents: fileDatas, paramater: paramater)
  }
  
  func getMultipartData(contents: [MultipartParamater], paramater: [String: Any]?) -> Data {
    let lineBreak = "\r\n"
    var data = Data()
    
    let paramCount = paramater?.count ?? 0
    if let parameter = paramater, paramCount > 0 {
      for (index, dictionary) in parameter.enumerated() {
        let boundary = "--\(allowedBoundary)\(lineBreak)".data(using: .utf8)
        let contentInfo = "Content-Disposition: form-data; name=\(dictionary.key)\(lineBreak)\(lineBreak)".data(using: .utf8)
        // MARK: - [index < paramCount ? lineBreak : ""] condition for not adding lineBreak on last element (causing last value got extra whitespace to server side)
        let contentType = "\(dictionary.value)\(index < paramCount ? lineBreak : "")".data(using: .utf8)
        
        if let boundary = boundary {
          data.append(boundary)
        }
        
        if let contentInfo = contentInfo {
          data.append(contentInfo)
        }
        
        if let contentType = contentType {
          data.append(contentType)
        }
      }
    }
    
    for content in contents {
      let boundary = "--\(allowedBoundary)\(lineBreak)".data(using: .utf8)
      let contentInfo = "Content-Disposition: form-data; name=\(content.apiFieldKey); filename=\(content.fileName)\(content.mimeType.fileExtension)\(lineBreak)".data(using: .utf8)
      let contentType = "Content-Type: \(content.mimeType.string)\(lineBreak)\(lineBreak)".data(using: .utf8)
      
      if let boundary = boundary {
        data.append(boundary)
      }
      
      if let contentInfo = contentInfo {
        data.append(contentInfo)
      }
      
      if let contentType = contentType {
        data.append(contentType)
      }
      
      data.append(content.file)
    }
    
    let closingBoundary = "\(lineBreak)--\(allowedBoundary)--\(lineBreak)".data(using: .utf8)
    if let closingBoundary = closingBoundary {
      data.append(closingBoundary)
    }
    
    return data
  }
}

public class MultipartParamater {
  var file: Data
  var fileName: String
  var apiFieldKey: String
  var mimeType: MimeType
  
  public init(file: Data, fileName: String, apiFieldKey: String, mimeType: MimeType) {
    self.file = file
    self.fileName = fileName
    self.apiFieldKey = apiFieldKey
    self.mimeType = mimeType
  }
}
