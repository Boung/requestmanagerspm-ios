//
//  Request.swift
//  
//
//  Created by Ly Boung on 22/8/23.
//

import UIKit

protocol Request {
  var showRequestLog: Bool { get }
  var loadingViewController: UIViewController { get }
  
  func cancelTask()
}
  
extension Request {
  func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
    if let nav = viewController as? UINavigationController {
      return topViewController(nav.visibleViewController)
    }
    if let tab = viewController as? UITabBarController {
      if let selected = tab.selectedViewController {
        return topViewController(selected)
      }
    }
    if let presented = viewController?.presentedViewController {
      return topViewController(presented)
    }
    
    return viewController
  }
  
  func showRequestLog(_ urlRequest: URLRequest, data: Data?, statusCode: Int?) {
    guard showRequestLog else { return }
    
    let url = urlRequest.url?.absoluteString ?? ""
    let method = urlRequest.httpMethod ?? ""
    let header = (urlRequest.allHTTPHeaderFields ?? [:]).toPrettyJson()
    var body = "nil"
    
    if let bodyData = urlRequest.httpBody, let bodyDictionary = try? JSONSerialization.jsonObject(with: bodyData, options: []) as? [String: Any] {
      body = bodyDictionary.toPrettyJson()
    }
    
    if let data = data, let response = prepareResponseLog(data: data) {
      let log = """
    
    ⏬️ Responding ==================================================
    [URL]: \(url)
    [StatusCode]: \(statusCode ?? 0)
    [Header]: \(header)
    [Method]: \(method)
    [Body]: \(body)
    [Response]: \(response)
    ================================================================
    """
      
      print(log)
      return
    }
    
    let log = """
    
    ⏫️ Requesting ==================================================
    [URL]: \(url)
    [Header]: \(header)
    [Method]: \(method)
    [Body]: \(body)
    ================================================================
    """
    
    print(log)
  }
  
  func prepareResponseLog(data: Data) -> String? {
    guard let jsonString = String(data: data, encoding: .utf8) else { return nil }
    guard let jsonData = jsonString.data(using: .utf8) else { return nil }
    
    if let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
      return json.toPrettyJson()
    }
    
    if let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [[String: Any]] {
      return "[\n\(json.map { $0.toPrettyJson() }.joined(separator: ",\n"))\n]"
    }
    
    return nil
  }
  
  func showLoadingController(_ bool: Bool, completion: Callback?) {
    guard bool else {
      completion?()
      return
    }
    
    DispatchQueue.main.async {
      if self.loadingViewController.isModal {
        completion?()
        return
      }
      
      guard let topVC = self.topViewController() else { return }
      self.loadingViewController.modalPresentationStyle = .overFullScreen
      self.loadingViewController.modalTransitionStyle = .crossDissolve
      
      topVC.present(self.loadingViewController, animated: true, completion: completion)
    }
  }
  
  func hideLoadingController(_ bool: Bool, completion: Callback?) {
    DispatchQueue.main.async {
      if bool {
        if self.loadingViewController.isModal {
          self.loadingViewController.dismiss(animated: true, completion: completion)
        }else {
          completion?()
        }
      }else {
        completion?()
      }
    }
  }
}
