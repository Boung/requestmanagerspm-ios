//
//  RequestProtocol.swift
//  
//
//  Created by Boung on 18/8/23.
//

import Foundation

public protocol RequestProtocol {
  var baseURL: String { get }
  var endPoint: String { get }
  var httpHeader: HTTPHeader? { get }
  var httpMethod: HTTPMethod { get }
  var paramater: Paramater? { get }
  var encoding: EncodingMethod { get }
  var requiredAuthentication: Bool { get }
  
  var timeoutInterval: TimeInterval { get }
}

public extension RequestProtocol {
  var timeoutInterval: TimeInterval { 60 }
}

public extension RequestProtocol {
  private var urlComponent: URLComponents? {
    guard let urlString = (baseURL + endPoint).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return nil }
    guard var urlComponent = URLComponents(string: urlString) else { return nil }
    
    if let paramater = paramater, encoding == .urlEncoding {
      urlComponent.queryItems = paramater.toURLQueryItems()
    }
    
    return urlComponent
  }
  
  var urlRequest: URLRequest? {
    guard let url = urlComponent?.url else { return nil }
    
    var request = URLRequest(url: url)
    request.timeoutInterval = timeoutInterval
    request.httpMethod = httpMethod.string
    request.allHTTPHeaderFields = httpHeader
    
    if let paramater = paramater, encoding == .jsonEncoding {
      if let data = try? JSONSerialization.data(withJSONObject: paramater, options: []) {
        request.httpBody = data
      }
    }
    
    return request
  }
}
