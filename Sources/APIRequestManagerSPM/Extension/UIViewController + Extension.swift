//
//  File.swift
//  
//
//  Created by Ly Boung on 18/8/23.
//

import UIKit

extension UIViewController {
  var isModal: Bool {
    let presentingIsModal = presentingViewController?.presentedViewController == self
    let presentingIsNavigation = navigationController != nil && navigationController?.presentingViewController?.presentedViewController == navigationController
    let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController
    
    return presentingIsModal || presentingIsNavigation || presentingIsTabBar
  }
}
