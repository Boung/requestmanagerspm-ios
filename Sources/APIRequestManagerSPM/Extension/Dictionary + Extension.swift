//
//  File.swift
//  
//
//  Created by Boung on 18/8/23.
//

import Foundation

extension Dictionary {
  func toURLQueryItems() -> [URLQueryItem] {
    return self.map { .init(name: "\($0.key)", value: "\($0.value)") }
  }
  
  func toPrettyJson() -> String {
    let errorString = "{ Pretty JSON failed }"
    
    guard let data = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted) else { return errorString }
    guard let json = String(data: data, encoding: .utf8) else { return errorString }
    
    return json
  }
}
