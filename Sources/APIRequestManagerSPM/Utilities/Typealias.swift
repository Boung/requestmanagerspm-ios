//
//  Typealias.swift
//  
//
//  Created by Ly Boung on 18/8/23.
//

import Foundation

public typealias HTTPHeader = [String: String]

public typealias Paramater = [String: Any]

public typealias Callback = (() -> Void)

public typealias CallbackType<T> = ((T) -> Void)
