//
//  ServerCode.swift
//  
//
//  Created by Ly Boung on 18/8/23.
//

import Foundation

struct ServerCode {
  static let statusOk = 200...202
  static let badRequest = 400
  static let unauthorized = 401
  static let notFound = 404
  static let internalServerError = 500
}
